import React, { useState } from 'react';
import AuthContext from "./AuthContext";
import {users} from "./users";

const AuthProvider = ({
    children
}) => {
    const [currentUser, setCurrentUser] = useState(() => {
        return JSON.parse(localStorage.getItem('userInfo'))
    });

    const authorize = ({ login, password }) => {
        const authorizedUserIndex = users.findIndex(
            (user) => user.login === login
        );

        if (authorizedUserIndex !== -1) {
            const authorizedUser = users[authorizedUserIndex];
            setCurrentUser(authorizedUser);

            localStorage.setItem('userInfo', JSON.stringify(authorizedUser));
        }

        return null;
    }

    const unauthorize = () => {
        setCurrentUser(null);
        localStorage.setItem('userInfo', null);
    }

    return (
        <AuthContext.Provider value={{
            currentUser,
            authorize,
            unauthorize,
        }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthProvider;
