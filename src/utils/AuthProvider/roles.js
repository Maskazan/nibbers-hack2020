export const Roles = {
    0: 'Doctor',
    1: 'Intern',
    2: 'Vendor',
    3: 'Specialist',
    Doctor: 0,
    Intern: 1,
    Vendor: 2,
    Specialist: 3,
};
