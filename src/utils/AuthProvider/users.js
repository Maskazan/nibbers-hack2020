import { Roles } from './roles';

export const users = [
    {
        login: 'doctor',
        password: 'osas',
        role: Roles.Doctor,
    },
    {
        login: 'intern',
        password: 'osas',
        role: Roles.Intern,
    },
    {
        login: 'specialist',
        password: 'osas',
        role: Roles.Specialist,
    },
    {
        login: 'vendor',
        password: 'osas',
        role: Roles.Vendor,
    },
]
