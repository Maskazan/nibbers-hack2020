import './style.scss'

import {Button, Col, Divider, Progress, Rate, Row, Typography} from 'antd'
import { useCallback, useState } from 'react';
import { Link } from 'react-router-dom';

import ContentCard from '../../components/ContentCard';
import XRayEditor from '../../components/XRayEditor';

const gutter = [16, 16];

const useProgress = (stepTimeout, cb) => {
    const [progress, setProgress] = useState(0);

    const start = useCallback(() => {
        setProgress(0);

        const intervalId = setInterval(() => {
            setProgress((progress) => {
                progress += Math.round(10 + 5 * Math.random());
                if (progress >= 100) {
                    progress = 100;
                    cb();
                    clearInterval(intervalId);
                }

                return progress;
            })
        }, stepTimeout);
    });

    return [progress, start];
}

const XRayEditorContainer = () => {
    const [polygons, setPolygons] = useState([]);
    const [saved, setSaved] = useState(false);
    const [comparing, setComparing] = useState(false);
    const [rate, setRate] = useState(false);

    const [progress, start] = useProgress(400, () => setComparing(true));

    return (
        <div className="xray-editor__wrapper">
            <ContentCard>
                <Row>
                    <Col span={comparing ? 12 : 24} className="xray-editor__col">
                        <XRayEditor
                            polygons={polygons}
                            onPolygonsChange={setPolygons}
                            fixed={saved}
                            rotatedTo='right'
                        />
                    </Col>
                    {comparing && <Col span={12} className="xray-editor__col">
                        <XRayEditor
                            polygons={polygons}
                            onPolygonsChange={setPolygons}
                            fixed={saved}
                            rotatedTo='left'
                            withCollision={true}
                        />
                    </Col>}
                </Row>
                <Divider/>
                <Row gutter={gutter}>
                    {
                        !saved && (
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: '100%',
                                }}
                            >
                                <Button
                                    onClick={() => { setSaved(true); start() }}
                                    type="primary"
                                >
                                    Сохранить и сравнить с системой
                                </Button>
                            </div>
                        )
                    }
                    {
                        (saved && !comparing) && (
                            <Progress percent={progress} />
                        )
                    }
                    {
                        (comparing && !rate) && (
                            <>
                                <Col span={12}>
                                    <div
                                        style={{
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Button
                                            onClick={() => setRate(true)}
                                            size="large"
                                        >
                                            Выбрать свою разметку
                                        </Button>
                                    </div>
                                </Col>
                                <Col span={12}  >
                                    <div
                                        style={{
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Link to='/patient-card'>
                                            <Button size="large">
                                                Выбрать разметку системы
                                            </Button>
                                        </Link>
                                    </div>
                                </Col>
                            </>
                        )
                    }
                    {
                        rate && (
                            <Col
                                span={12}
                                push={12}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'column',
                                    }}
                                >
                                    <Typography.Title level={4}>Поставьте оценку работе системы</Typography.Title>
                                    <Rate />
                                    <Link to="/patient-card">
                                        <Button
                                            type="primary"
                                            style={{
                                                marginTop: 16,
                                            }}
                                        >
                                            Отправить
                                        </Button>
                                    </Link>
                                </div>
                            </Col>

                        )
                    }
                </Row>
            </ContentCard>
        </div>
    );
};

export default XRayEditorContainer;
