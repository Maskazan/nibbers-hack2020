import React, { useState } from 'react';
import {Col, Divider, Input, Row, Typography, Select, Button} from "antd";
import Card from "../../components/ContentCard";
import './Patients.scss';
import {Link} from "react-router-dom";
import patients from "../../data/patients";

const Patients = () => {
    const [filterValue, setFilterValue] = useState('');

    const filteredPatients = patients.filter((patient) => patient.name.includes(filterValue)).slice(0, 10);

    return (
        <div className="patients">
            <Typography.Title>Выберите пациента</Typography.Title>

            <Row gutter={[32, 16]}>
                <Col span={16}>
                    <Card>
                        <Input
                            onChange={(event) => setFilterValue(event.target.value)}
                            size="large"
                            style={{ marginBottom: 8 }}
                        />
                        <Typography.Text type="secondary">
                            Введите минимум 4 символа фамилии, номера паспорта или карты
                        </Typography.Text>
                        <Typography.Link
                            style={{
                                float: 'right',

                            }}
                        >
                            Расширенный поиск
                        </Typography.Link>
                        <Divider />
                        <div className="patients-list">
                            {
                                filteredPatients.map((patient) => {
                                    return (
                                        <div className="patient-item">
                                            <Link to="/patient-card" >
                                                {patient.name}
                                            </Link>
                                            <br />
                                            <Typography.Text type="secondary">
                                                {patient.cardNumber}
                                            </Typography.Text>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Typography.Title level={3} style={{
                            marginBottom: 24,
                        }}>
                            Сортировка
                        </Typography.Title>
                        <Select
                            value="По алфавиту"
                            style={{
                                width: '100%',
                            }}
                        />
                    </Card>
                    <Card>
                        <Typography.Title level={3} style={{ marginBottom: 16 }}>
                            Новый пациент
                        </Typography.Title>
                        <Typography.Text>
                            Необходимо добавить пациента?
                            <br />
                            Нажмите на кнопку ниже и заполните форму.
                        </Typography.Text>
                        <Button
                            type="primary"
                            block
                            style={{
                                marginTop: 32,
                            }}
                        >
                            Добавить нового пациента
                        </Button>
                    </Card>
                </Col>
            </Row>
        </div>
    )
};

export default Patients;
