import './style.scss';

import { Col, List, Row, Typography } from "antd";
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import Card from '../../components/ContentCard'
import ava from './ava.png';
import xray1 from './xray/1.png'
import xray2 from './xray/2.png'
import xray3 from './xray/3.png'
import xray4 from './xray/4.png'

const { Title, Text } = Typography;

const gutter = [32, 16];

const PatientCard = () => {

  return (
    <Row gutter={gutter}>
      <Col span={16}>
        <Card>
          <Row gutter={gutter}>
            <Col span={20}><Title>Иванов Иван Иванович</Title></Col>
            <Col span={4}><img className="patient-card__ava" src={ava} alt="Иванов Иван Иванович" /></Col>
          </Row>
          <Row gutter={gutter}>
            <Col span={8}><Text type="secondary">Номер карты</Text></Col>
            <Col span={16}><Text strong>333-921</Text></Col>
          </Row>
          <Row gutter={gutter}>
            <Col span={8}><Text type="secondary">Год рождения</Text></Col>
            <Col span={16}><Text strong>31 октября 1975, 45 лет</Text></Col>
          </Row>
          <Row gutter={gutter}>
            <Col span={8}><Text type="secondary">Адрес проживания</Text></Col>
            <Col span={16}><Text strong>РФ, Казань, ул. Патриса Лумумбы, 47-203</Text></Col>
          </Row>
          <Row gutter={gutter}>
            <Col span={8}><Text type="secondary">Номер телефона</Text></Col>
            <Col span={16}><Text strong>+7 623 829-99-03</Text></Col>
          </Row>
          <Row gutter={gutter}>
            <Col span={8}><Text type="secondary">Электронная почта</Text></Col>
            <Col span={16}><Text strong>нет</Text></Col>
          </Row>
          <Row gutter={gutter}>
            <Col span={8}><Text type="secondary">Место работы</Text></Col>
            <Col span={16}><Text strong>ООО “Трансмашхолдинг”, Диреция, Водитель электровоза</Text></Col>
          </Row>
        </Card>
        <Card>
          <Title level={3} style={{ marginBottom: 24 }}>Рентген</Title>
          <Link to="/xray-editor" className='patient-card__xray' ><img alt="1" src={xray1} /></Link>
          <Link to="/xray-editor" className='patient-card__xray' ><img alt="2" src={xray2} /></Link>
          <Link to="/xray-editor" className='patient-card__xray' ><img alt="3" src={xray3} /></Link>
          <Link to="/xray-editor" className='patient-card__xray' ><img alt="4" src={xray4} /></Link>
        </Card>
      </Col>
      <Col span={8}>
        <Card>
          <Title level={3}>Навигация</Title>
          <List bordered={false} split={false} >
            <List.Item><Link>Приемы</Link></List.Item>
            <List.Item><Link>Программы</Link></List.Item>
            <List.Item><Link>Документы</Link></List.Item>
            <List.Item><Link>История болезни</Link></List.Item>
          </List>
        </Card>
      </Col>
    </Row>
  )
}

export default PatientCard;
