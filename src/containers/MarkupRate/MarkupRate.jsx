import React from 'react';
import ContentCard from "../../components/ContentCard";
import {Button, Col, Divider, Input, Rate, Row, Select, Typography} from "antd";
import {Link} from "react-router-dom";
import XRayEditor from "../../components/XRayEditor";
import predefinedXrayState from "./predefinedXrayState";

const MarkupRate = () => {
    return (
        <div className="markup-rate">
            <Typography.Title>Оценка работы алгоритмов по разметке изображений</Typography.Title>

            <Row gutter={[32, 16]}>
                <Col span={16}>
                    <ContentCard>
                        <XRayEditor
                            fixed
                            polygons={predefinedXrayState}
                        />
                    </ContentCard>
                </Col>
                <Col span={8}>
                    <ContentCard>
                        <Typography.Title level={3} >
                            Поставьте оценку
                        </Typography.Title>
                        <Rate
                            style={{
                                marginBottom: 24,
                            }}
                        />
                        <Button
                            type="primary"
                            size="large"
                            block
                        >
                            Отправить и перейти дальше
                        </Button>
                    </ContentCard>
                    <ContentCard className="additional-info">
                        <Typography.Title level={3}>
                            Дополнительная информация
                        </Typography.Title>

                        <Link
                            to="/patient-card"
                            // component={Typography.Link}
                        >
                            Карта пациента
                        </Link>
                        <Link
                            to="/xray.png"
                            target="_blank"
                            component={Typography.Link}
                        >
                            Оригинал изображения
                        </Link>
                    </ContentCard>
                </Col>
            </Row>
        </div>
    );
};

export default MarkupRate;
