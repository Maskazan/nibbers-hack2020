import React from 'react';
import {Col, Row, Typography, Input, Form, Button, Divider} from "antd";
import ContentCard from "../../components/ContentCard";
import { Link } from 'react-router-dom';

const NeuralNetRegistration = () => {
    return (
        <div className="neural-net-registration">
            <Typography.Title>Подключение к системе</Typography.Title>

            <Row gutter={[32, 16]}>
                <Col span={16}>
                    <ContentCard>
                        <Form>
                            Для того, чтобы подключить алгоритм к системе. Вам нужно обеспечить сервер,
                            который будет реализовывать Callback API, согласно&nbsp;
                            <Link
                                to="/register-doc.pdf"
                                component={Typography.Link}
                            >
                                нашей документации
                            </Link>
                            . Далее нужно в форме ниже указать адрес точки входа, на котором этот API реализован.

                            <Divider />
                            <Form.Item
                                name="nnregister-entry-point-url"
                            >
                                <Input
                                    size="large"
                                    placeholder="https://server.com/api"
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size="large"
                                    style={{
                                        borderRadius: '4px',
                                        height: '40px',
                                    }}
                                    value="login"
                                >
                                    Отправить
                                </Button>
                            </Form.Item>
                        </Form>
                    </ContentCard>
                </Col>
            </Row>
        </div>
    )
};

export default NeuralNetRegistration;
