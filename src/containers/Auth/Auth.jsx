import React, { useContext } from 'react';
import {Select, Form, Button, Layout, Typography, Anchor} from "antd";
import { AuthContext } from "../../utils/AuthProvider";
import './Auth.style.scss';
import {Link} from "react-router-dom";
import MainTemplate from "../../components/MainTemplate";

const Auth = () => {
    const { authorize } = useContext(AuthContext);

    const onFinish = (credentials) => {
        authorize(credentials)
    }

    return (
        <MainTemplate
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <div className="auth-form">
                <Form
                    name="basic"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                >
                    <div className="auth-form__heading">
                        <Typography.Title level={3}>Добро пожаловать</Typography.Title>
                    </div>

                    <Form.Item
                        name="login"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Select
                            size="large"
                            placeholder="Выберите роль"
                            style={{
                                borderRadius: '4px',
                            }}
                        >
                            <Select.Option value="doctor">Врач</Select.Option>
                            <Select.Option value="intern">Интерн</Select.Option>
                            <Select.Option value="vendor">Вендор</Select.Option>
                            <Select.Option value="specialist">Эксперт</Select.Option>

                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button
                            type="primary"
                            htmlType="submit"
                            block
                            size="large"
                            style={{
                                borderRadius: '4px',
                                height: '40px',
                            }}
                            value="login"
                        >
                            Войти
                        </Button>
                    </Form.Item>
                    <div className="auth-form__links">
                        <Link>Регистрация</Link>
                        <Link>Забыл пароль</Link>
                    </div>
                </Form>
            </div>
        </MainTemplate>
    )
}

export default Auth;
