import './App.scss';
import MainRoute from "./routes/MainRoute";
import AuthProvider from "./utils/AuthProvider";

import 'antd/dist/antd.css';

function App() {
    return (
        <div className="app">
            <AuthProvider>
                <MainRoute />
            </AuthProvider>
        </div>
    );
}

export default App;
