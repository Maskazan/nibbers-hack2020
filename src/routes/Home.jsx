import React, {useContext} from 'react';
import {Link} from "react-router-dom";
import {Button} from "antd";
import { AuthContext } from "../utils/AuthProvider";

const Home = () => {
    const { unauthorize } = useContext(AuthContext);

    return (
        <div>
            Главная
            <br />
            <Link to="/upload">
                К загрузке данных
            </Link>
        </div>
    );
};

export default Home;
