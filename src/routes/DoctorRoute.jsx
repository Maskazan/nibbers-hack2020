import {Route, Switch} from "react-router-dom";

import MainTemplate from "../components/MainTemplate";
import PatientCard from '../containers/PatientCard'
import Patients from "../containers/Patients/Patients";
import React from 'react';
import Upload from "../containers/Upload";
import XRayEditor from '../containers/XRayEditor'

const DoctorRoute = () => {
    return (
        <MainTemplate header>
            <Switch>
                <Route path="/" component={Patients} exact />
                <Route path="/upload" component={Upload} />
                <Route path="/patient-card" component={PatientCard} />
                <Route path="/xray-editor" component={XRayEditor} />
            </Switch>
        </MainTemplate>
    )
};

export default DoctorRoute;
