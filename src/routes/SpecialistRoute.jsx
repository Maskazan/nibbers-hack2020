import React from 'react';
import MainTemplate from "../components/MainTemplate";
import { Route, Switch } from 'react-router-dom';
import MarkupRate from "../containers/MarkupRate";
import PatientCard from "../containers/PatientCard";

const SpecialistRoute = () => {
    return (
        <MainTemplate header>
            <Switch>
                <Route exact path="/" component={MarkupRate} />
                <Route path="/patient-card" component={PatientCard} />
            </Switch>

        </MainTemplate>
    )
};

export default SpecialistRoute;
