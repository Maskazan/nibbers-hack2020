import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Home from "./Home";
import UploadRoute from "./Upload";
import AuthRoute from "./Auth";
import { AuthContext } from "../utils/AuthProvider";
import {Roles} from "../utils/AuthProvider/roles";
import DoctorRoute from "./DoctorRoute";
import {Button} from "antd";
import SpecialistRoute from "./SpecialistRoute";
import VendorRoute from "./VendorRoute";
import InternRoute from "./InternRoute";


const MainRoute = () => {
    const { currentUser, unauthorize } = useContext(AuthContext);

    console.log(currentUser)

    return (
        <Router>
            <Switch>
                <Route path="/auth" component={AuthRoute} />
                {
                    !currentUser && (
                        <Redirect to="/auth" />
                    )
                }
                <Route path="/">
                    <Switch>
                        {
                            currentUser?.role === Roles.Doctor && (
                                <DoctorRoute />
                            )
                        }
                        {
                            currentUser?.role === Roles.Intern && (
                                <InternRoute />
                            )
                        }
                        {
                            currentUser?.role === Roles.Specialist && (
                                <SpecialistRoute />
                            )
                        }
                        {
                            currentUser?.role === Roles.Vendor && (
                                <VendorRoute />
                            )
                        }
                        <Route path="/" component={Home} />
                    </Switch>
                </Route>

            </Switch>
        </Router>
    );
}

export default MainRoute;
