import React from 'react';
import MainTemplate from "../components/MainTemplate";
import {Route, Switch} from 'react-router-dom';
import Patients from "../containers/Patients/Patients";
import PatientCard from "../containers/PatientCard";

const InternRoute = () => {
    return (
        <MainTemplate header>
            <Switch>
                <Route path="/" component={Patients} exact />
                <Route path="/patient-card" component={PatientCard} />
            </Switch>
        </MainTemplate>
    )
};

export default InternRoute;
