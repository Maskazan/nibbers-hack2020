import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Upload from "../containers/Upload";

const UploadRoute = () => {
    return (
        <Switch>
            <Route path="/upload" component={Upload} />
        </Switch>
    );
};

export default UploadRoute;
