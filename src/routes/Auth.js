import React, { useContext } from 'react';
import Auth from "../containers/Auth";
import { Route, Redirect, Switch } from 'react-router-dom';
import { AuthContext } from "../utils/AuthProvider";

const AuthRoute = () => {
    const { currentUser } = useContext(AuthContext);
    return (
        <Switch>
            {
                currentUser && (
                    <Redirect to="/" />
                )
            }
            <Route path="/" component={Auth} />
        </Switch>
    );
};

export default AuthRoute;
