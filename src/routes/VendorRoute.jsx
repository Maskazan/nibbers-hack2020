import React from 'react';
import MainTemplate from "../components/MainTemplate";
import { Route } from 'react-router-dom';
import NeuralNetRegistration from "../containers/NeuralNetRegistration";

const VendorRoute = () => {
    return (
        <MainTemplate header>
            <Route render={() => (
                <NeuralNetRegistration />
            )} />
        </MainTemplate>
    )
};

export default VendorRoute;
