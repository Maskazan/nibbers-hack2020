import './style.scss'

const ContentCard = ({ children, className='' }) => {

  return (
    <div className={className + ' content-card'}>
      {children}
    </div>
  )
}

export default ContentCard;
