/* eslint-disable jsx-a11y/alt-text */
import { useCallback, useMemo, useState } from 'react';

import img from './img.png';

const pushPointer = ({ x, y }) => (polygon) => {
  const point = `${x},${y}`;

  if (polygon.length === 0) {
    return point;
  }

  return `${polygon} ${point}`
}

const eventToPointer = (e) => {
  const { offsetX: x, offsetY: y } = e.nativeEvent;
  return { x, y };
}

const getEmptyPolygon = () => '';
const isPolygonEmpty = (polygon) => polygon.length === 0;

const getUndefinedPointer = () => ({ x: -1, y: -1 });
const isPointersClose = (a, b) => (Math.abs(a.x - b.x) + Math.abs(a.y - b.y)) < 10;
const isPointerDefined = ({ x, y }) => x >= 0 && y >= 0;

const pushPolygon = (polygon) => (polygons) => [...polygons, polygon];

const polygonStyle = { fill: 'url(\'#logo-gradient\')', stroke: '#ccc', strokeWidth: 1, fillOpacity: '0.1' };
const polygonWithCollisionStyle = { fill: 'red', stroke: '#ccc', strokeWidth: 1, fillOpacity: '0.1' };
const targetPolygonStyle = { fill: 'url(\'#logo-gradient\')', stroke: 'white', strokeWidth: 1, fillOpacity: '0.1' };

const editorStyle = {
  position: 'relative',
  perspective: 600,
  width: 512,
  height: 512
}

const svgStyle = {
  position: 'absolute',
  left: 0, top: 0,
  borderRadius: 7,
  transition: 'transform 1s',
  transformStyle: 'preserve-3d',
  transform: 'rotateY(0deg) rotateX(0deg)'
};

const svgStyleRotatedToRight = {
  ...svgStyle,
  transform: 'rotateY(7deg) rotateX(7deg)'
}

const svgStyleRotatedToLeft = {
  ...svgStyle,
  transform: 'rotateY(-7deg) rotateX(7deg)'
}

const imgStyle = { ...svgStyle };
const imgStyleRotatedToRight = { ...svgStyleRotatedToRight };
const imgStyleRotatedToLeft = { ...svgStyleRotatedToLeft };

const circleStyle = {
  stroke: 'white',
  strokeWidth: 1,
  fillOpacity: 0
}

const randDelta = () => 30 * Math.random() - 60;

const XRayEditor = ({ polygons, onPolygonsChange, fixed, rotatedTo = 'right', withCollision = false }) => {
  const [targetPolygon, setTargetPolygon] = useState('');
  const [startPointer, setStartPointer] = useState(getUndefinedPointer());
  const [currPointer, setCurrPointer] = useState(getUndefinedPointer());

  const sectionsStyle = useMemo(() => ({
    ...imgStyle,
    clipPath: withCollision ? 'url(#clipping-wc)' : 'url(#clipping)',
    transform: 'rotateY(0deg) rotateX(0deg) translateZ(0px)'
  }), [withCollision]);

  const sectionsStyleRotatedToRight = useMemo(() => ({
    ...sectionsStyle,
    transform: 'rotateY(7deg) rotateX(7deg) translateZ(70px)'
  }), [sectionsStyle]);

  const sectionsStyleRotatedToLeft = useMemo(() => ({
    ...sectionsStyle,
    transform: 'rotateY(-7deg) rotateX(7deg) translateZ(70px)'
  }), [sectionsStyle]);

  const [stablePolygons, polygonsWithCollision] = useMemo(() => {
    if (polygons.length === 0) return [[], []];

    let stablePolygons = [...polygons];
    let polygonsWithCollision = [];

    const collisionCount = 1 + Math.round(polygons.length * 0.3 * Math.random());

    for (let i = 0; i < collisionCount; i += 1) {
      const index = Math.round(Math.random() * (stablePolygons.length - 1));
      polygonsWithCollision.push(...stablePolygons.splice(index, 1));
    }

    polygonsWithCollision = polygonsWithCollision
      .map(
        (polygon) => polygon
          .split(' ')
          .map((point) => point.split(','))
          .map(([x, y]) => ([parseFloat(x) + randDelta(), parseFloat(y) + randDelta()]))
          .map(([x, y]) => `${x},${y}`)
          .join(' ')
      );

    console.log(stablePolygons, polygonsWithCollision);

    return [stablePolygons, polygonsWithCollision]
  }, [polygons])

  const handleMouseMove = useCallback(
    (e) => {
      if (fixed) return;
      setCurrPointer(eventToPointer(e))
    },
    [setCurrPointer, fixed]
  );

  const tmpTargetPolygon = pushPointer(currPointer)(targetPolygon);

  const handleClick = useCallback((e) => {
    if (fixed) return;

    const currPointer = eventToPointer(e);

    if (isPointersClose(startPointer, currPointer)) {
      onPolygonsChange(pushPolygon(targetPolygon));
      setStartPointer(getUndefinedPointer());
      setTargetPolygon(getEmptyPolygon());
    } else {
      if (isPolygonEmpty(targetPolygon)) {
        setStartPointer(currPointer);
      }
      setTargetPolygon(pushPointer(currPointer))
    }
  }, [startPointer, targetPolygon, fixed, onPolygonsChange, setStartPointer, setTargetPolygon]);

  let polygonComponents = useMemo(() =>
    polygons.map((polygonPoints, index) => (
      <polygon
        key={index}
        points={polygonPoints}
        style={polygonStyle}
      />
    )),
    [polygons]
  )

  const polygonComponentsWithCollision = useMemo(() =>
    [...polygonsWithCollision.map((polygonPoints, index) => (
      <polygon
        key={index + 100}
        points={polygonPoints}
        style={polygonWithCollisionStyle}
      />
    )), ...stablePolygons.map((polygonPoints, index) => (
      <polygon
        key={index + 200}
        points={polygonPoints}
        style={polygonStyle}
      />
    ))],
    [polygonsWithCollision, stablePolygons]
  )

  polygonComponents = withCollision ? polygonComponentsWithCollision : polygonComponents;

  return (
    <div style={editorStyle}>
      <img
        src={img}
        width={512}
        height={512}
        style={fixed ? (rotatedTo === 'right' ? imgStyleRotatedToRight : imgStyleRotatedToLeft) : imgStyle}
      />
      <svg
        width="512"
        height="512"
        style={{ ...(fixed ? (rotatedTo === 'right' ? imgStyleRotatedToRight : imgStyleRotatedToLeft) : imgStyle), filter: 'url(#blurFilter)' }}
      >
        {polygonComponents}
      </svg>
      <img
        src={img}
        width={512}
        height={512}
        style={fixed ? (rotatedTo === 'right' ? sectionsStyleRotatedToRight : sectionsStyleRotatedToLeft) : sectionsStyle} />
      <svg
        onClick={handleClick}
        onMouseMove={handleMouseMove}
        width="512"
        height="512"
        style={fixed ? (rotatedTo === 'right' ? svgStyleRotatedToRight : svgStyleRotatedToLeft) : svgStyle}
      >
        <defs>
          <linearGradient id="logo-gradient" x1="50%" y1="0%" x2="50%" y2="100%" >
            <stop offset="0%" stopColor="#7A5FFF">
              <animate attributeName="stopColor" values="#7A5FFF; #01FF89; #7A5FFF" dur="8s" repeatCount="indefinite"></animate>
            </stop>
            <stop offset="100%" stopColor="#01FF89">
              <animate attributeName="stopColor" values="#01FF89; #7A5FFF; #01FF89" dur="8s" repeatCount="indefinite"></animate>
            </stop>
          </linearGradient>

          <filter id="blurFilter">
            <feGaussianBlur in="SourceGraphic" stdDeviation="2" />
          </filter>
        </defs>
        <clipPath id={withCollision ? 'clipping-wc' : 'clipping'}>
          {polygonComponents}
        </clipPath>
        <polygon
          points={tmpTargetPolygon}
          style={targetPolygonStyle}
        />
        {!fixed && isPointerDefined(startPointer) && (
          <circle cx={startPointer.x} cy={startPointer.y} r={5} style={circleStyle} />
        )}
      </svg>
      <svg
        width="512"
        height="512"
        style={fixed ? (rotatedTo === 'right' ? sectionsStyleRotatedToRight : sectionsStyleRotatedToLeft) : sectionsStyle}
      >
        {polygonComponents}
      </svg>
    </div>
  )
}

export default XRayEditor;