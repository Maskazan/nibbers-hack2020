# XRayEditor

## Props:
* **fixed** - состояние сохранения (true | false)
* **polygons** - массив строк \[polygons] с точками для svg:polygon
* **onPolygonsChange** - (polygons) => {} cb с новым списком polygons

```
function App() {
  const [polygons, setPolygons] = useState([]);
  const [fixed, setFixed] = useState(false);
  return (
    <div className="App">
      <XRayEditor
        polygons={polygons}
        onPolygonsChange={setPolygons}
        fixed={fixed}
      />
      <button onClick={() => setFixed(!fixed)}>Save</button>
    </div>
  );
}
```