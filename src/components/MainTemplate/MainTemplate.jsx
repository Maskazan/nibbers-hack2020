import './MainTemplate.scss';

import {Button, Image, Layout, Typography} from "antd";

import React, { useContext } from 'react';
import {Container} from "../Grid";
import Logo from '../../Assets/images/logo.png';
import {AuthContext} from "../../utils/AuthProvider";
import { useHistory } from 'react-router-dom';

const { Content } = Layout;

const MainTemplate = ({ children, style, header }) => {
    const { unauthorize } = useContext(AuthContext);
    const history = useHistory();

    return (
        <Layout
            style={{
                minHeight: '100vh',
                display: 'block',
                paddingBottom: '32px',
                position: 'relative',
            }}
        >
            {
                header && (
                    <Layout.Header
                        style={{
                            backgroundColor: '#fff',
                        }}
                    >
                        <Container>
                            <Typography.Title
                                level={2}
                                style={{
                                    lineHeight: '64px',
                                    display: 'inline-block',
                                    cursor: 'pointer',
                                }}
                                onClick={() => {
                                    history.push('/');
                                }}
                            >
                                Nibbers
                            </Typography.Title>
                            <Image
                                src={Logo}
                                style={{
                                    verticalAlign: 'top',
                                    marginLeft: 16,
                                }}
                            />
                            <div className="btn-unauthorize">
                                <Button
                                    onClick={unauthorize}
                                >
                                    Выйти
                                </Button>
                            </div>
                        </Container>
                    </Layout.Header>
                )
            }
            <Content
                style={{
                    maxWidth: 1120,
                    minWidth: 1120,
                    margin: '0 auto',
                    paddingTop: 32,
                    ...style
                }}
            >
                { children }
            </Content>
            <div className="copyright">
                <Typography.Text type="secondary">
                    2020 © Nibbers team
                </Typography.Text>
            </div>
        </Layout>
    )
}

export default MainTemplate;
